from http import HTTPStatus

from flask import Flask, request, jsonify
from flask_swagger import swagger

from challenge.trie import load_data

app = Flask(__name__)


@app.before_first_request
def load_csv():
    app.trie = load_data()


@app.route('/', methods=['GET'])
def index():
    return 'It Works!'


@app.route('/autocomplete', methods=['GET'])
def autocomplete():
    """
    Find app name in data trie structure
    ---
    parameters:
      - name: q
        in: query
        type: string
        description: word to autocomplete
        required: true
    responses:
      200:
        description: json list of app names
      400:
        description: missing 'q'parameter
    """
    query = request.args.get('q')
    if query:
        results = app.trie.prefix(query)
        return jsonify(list(results))
    else:
        return 'bad request', HTTPStatus.BAD_REQUEST


@app.route("/spec")
def spec():
    swag = swagger(app)
    swag['host'] = 'aptoide-python.herokuapp.com'
    swag['info']['version'] = '0.0.1'
    swag['info']['title'] = 'Aptoid python challenge'
    with open('../description.txt', 'r') as text_file:
        swag['info']['description'] = text_file.read()
    return jsonify(swag)

