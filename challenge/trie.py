import os

from challenge.config import PROD


class TrieNode:

    def __init__(self):
        self.end = False
        self.children = dict()
        self.word = None

    def __repr__(self):
        return 'TrieNode: -> {}'.format(self.children)

    def words(self, prefix: str):
        if self.end:
            yield self.word
        for letter, child in self.children.items():
            yield from child.words(prefix + letter)


class Trie:

    def __init__(self):
        self.root = TrieNode()
        self.nodes = 1

    def __len__(self):
        return self.nodes

    def __getitem__(self, item):
        current = self.root
        for char in item:
            current = current.children.get(char)
            if current is None:
                return None
        return current

    def insert(self, word: str):
        current = self.root
        normalized = word.strip().lower()
        for char in normalized:
            node = current.children.get(char)
            if not node:
                node = TrieNode()
                current.children[char] = node
                self.nodes += 1
            current = node
        current.end = True
        current.word = word.strip()

    def prefix(self, prefix: str):
        prefix = prefix.lower()
        current = self[prefix]
        yield from current.words(prefix)


def load_data() -> Trie:
    filename = 'data/6500titles.csv' if PROD else 'data/190titles.csv'
    current_dir = os.path.dirname(os.path.realpath(__file__))
    filename = os.path.join(current_dir, filename)
    trie = Trie()
    with open(filename) as text_file:
        for line in text_file:
            trie.insert(line)
    return trie

