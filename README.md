# Python Challenge

## Description
Autocomplete API that helps users searching for apps by their name

## Requirements
Python 3.5.2+


## Easy way

Access [swaggerhub](https://app.swaggerhub.com/apis/lucasrc/aptoid-python_challenge/0.0.1)
The code is deployed on heroku server and swagger is the client/doc of API


## Usage (common way)
To run the server, please execute the following from the root directory:

```
pip install -r requirements.txt
make run
```

and open your browser to here:

```
http://localhost:8080/
```

if it show "It Works!" it's already running :-)


Swagger definition lives here:

```
http://localhost:8080/spec
```

To launch tests:
```
make test
```

## Running with Docker

To run the server on a Docker container, please execute the following from the root directory:

```bash
make run_docker
```