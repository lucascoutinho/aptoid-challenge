run:
	python -m challenge
run_docker:
	docker build -t aptoid_challenge .
	docker run -p 8080:8080 aptoid_challenge
test:
	python -m unittest discover
