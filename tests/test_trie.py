from challenge.trie import Trie, load_data
from tests.base import BaseTest


class TestTrie(BaseTest):

    def setUp(self):
        self.trie = Trie()

    def test_insert(self):
        self.trie.insert('ab')
        self.trie.insert('abc')
        self.trie.insert('bc')
        self.trie.insert('bcd')
        assert 'a' in self.trie.root.children.keys()
        assert 'b' in self.trie.root.children.keys()
        assert 'c' not in self.trie.root.children.keys()
        assert 'b' in self.trie.root.children['a'].children
        assert 'c' in self.trie.root.children['a'].children['b'].children
        assert len(self.trie) == 7

    def test_search(self):
        self.trie.insert('WhatsApp')
        self.trie.insert('whatsit')

        results = list(self.trie.prefix('wha'))
        assert results == ['WhatsApp', 'whatsit']


class TestTrieLoadData(BaseTest):

    def test_load_trie_tree(self):
        trie = load_data()
        assert isinstance(trie, Trie)

