from unittest import TestCase
import challenge


class BaseTest(TestCase):

    def __init__(self, method_name='runTest'):
        challenge.trie.PROD = False
        super(BaseTest, self).__init__(methodName=method_name)
