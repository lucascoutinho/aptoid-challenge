from challenge.api import app
from challenge.trie import Trie
from tests.base import BaseTest


class TestApi(BaseTest):

    def setUp(self):
        self.app = app.test_client()

    def test_trie_in_app(self):
        assert isinstance(self.app.application.trie, Trie)

    def test_search(self):
        response = self.app.get('/autocomplete?q=Apto')
        assert response.json == ['Aptoide Backup Apps', 'Aptoide Lite',
                                 'Aptoide Uploader']

    def test_search_missing_query(self):
        response = self.app.get('/autocomplete')
        assert response.status_code == 400
